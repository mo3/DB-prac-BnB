use Di14b_1;

drop procedure if exists addGast ;

DELIMITER //
create procedure addGast(IN lastname varchar(256), IN firstname varchar(45),
 IN address varchar(512))
begin
	declare num int;
 	-- check if a guest with the given name and address exists
	IF (select count(*) from Gast 
		where Gast.Vorname=firstname and Gast.Nachname=lastname
				and Gast.Addresse=address
		) > 0
	then
		-- return the existing one is so
        select * from Gast where Gast.Vorname=firstname and Gast.Nachname=lastname
            and Gast.Addresse=address;
	else
		START TRANSACTION
			-- create a new entry if not; the first 50 guest can get a discount of 10%
			set num = (select count(idGast) from Gast);
            if (num < 50) then
				insert into Gast Values (null, lastname, firstname, address, 0.1);
				-- if therer are to much discounts, revert the new entry
				if (select AVG(PreisProzente) from Gast) > 0.03
					ROLLBACK;
				end;
            else
				insert into Gast Values (null, lastname, firstname, address, 0);
			end;
        COMMIT;
	end if;
end//
