use Di14b_1;

revoke all on * from moe, D4rk;

drop user moe;
drop user D4rk; 

flush privileges;
/* user moe und dark werden erstellt */
create user moe identified by '8008135'; 
create user D4rk identified by 'cookies';

/* both users get maximum rights */
grant all on Di14b_1.* to moe, D4rk;
grant all on Di14b_1.Bewertungen to D4rk; 

/* remove update right form dark in  'Bewertungen' */
revoke update on Di14b_1.Bewertungen from D4rk;

show grants for moe;
show grants for D4rk;

revoke all on * from moe, D4rk;
revoke all on Di14b_1.Bewertungen

/* debugging stuff */
-- show tables;
