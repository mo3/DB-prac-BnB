USE Di14b_1;
/*Reset all changes done by the scipt during the last execution*/
update BuchungComplete set Nachname="Trump" where Gast_idGast=2;
DROP view BuchungComplete;
/*
Create a view that contains all relevant information from several tables.
*/
CREATE VIEW BuchungComplete AS
    SELECT 
        Buchung.*,
        Gast.Vorname,
        Gast.Nachname,
        Gast.Addresse,
        Gast.PreisProzente,
        Kontoverbindung.IBAN,
        Kontoverbindung.BIC,
        Kontoverbindung.Locked
    FROM
        Buchung
            JOIN
        Gast ON Buchung.Gast_idGast = Gast.idGast
            LEFT JOIN
        Kontoverbindung USING (Gast_idGast)
    ORDER BY Buchung.idBuchung;
    
/*
insert into BuchungComplete (Vorname, Nachname) values ('foo', 'bar');
->Fails due to Error Code: 1471. The target table BuchungComplete of the INSERT is not insertable-into
*/

/*Change the customer name from "Donald Trump" to "Donald Dump". (Changes the data in the "Gast" table as well)*/
update BuchungComplete set Nachname="Dump" where Gast_idGast=2;

/*Show all data from "Buchung" with additional usefull information from other tables.*/
SELECT 
    *
FROM
    BuchungComplete;
    
/*Reset all changes done by the scipt during the last execution*/    
Drop view wealthyCustomers;
/*Create a view that contains all "wealthy" clients (spent more then 200),
with their personal and payment information*/
CREATE VIEW wealthyCustomers AS
    SELECT 
        Gast_idGast, Vorname, Nachname, SUM(Preis), IBAN, BIC
    FROM
        Buchung
            JOIN
        Gast ON Buchung.Gast_idGast = Gast.idGast
            LEFT JOIN
        Kontoverbindung USING (Gast_idGast)
    GROUP BY Gast_idGast
    HAVING SUM(Preis) > 200;
    
select * from wealthyCustomers;
