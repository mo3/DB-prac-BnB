use Di14b_1;
-- override the procedure if it already exists
drop procedure if exists addBuchung;

DELIMITER //
create procedure addBuchung(
		IN start_date date, 
		IN end_date date,
		IN preis FLOAT,
        IN gast_id INT,
        IN wohnung_id INT,
        IN transfer_id INT
    )
begin
	START TRANSACTION;
		-- add a new transaction
		INSERT INTO Buchung (StartDate, EndDate, Preis, Gast_idGast, Wohnung_idWohnung, Transfer_idTransfer) 
			VALUES (start_date, end_date, preis, gast_id, wohnung_id, transfer_id);
		-- if the guest has no payment information, revert the new transaction and signal an error
		IF (SELECT COUNT(*) FROM Kontoverbindung WHERE Gast_idGast=gast_id) = 0 Then
			ROLLBACK;
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MISSING Kontoverbindung - NO INSERT!';
		END if;
		COMMIT;
end//
