import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/*
 * Anwendungsfall 2
 */

public class DBFerienwohnung {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out
				.println("Geben Sie das gewünschte Datum in der Form YYYY-MM-DD ein.");
		String eingabe = in.nextLine();

		// Valides Datum sicherstellen
		try {
			LocalDate.parse(eingabe);
		} catch (DateTimeParseException e) {
			System.out.println("Datum nicht korrekt eingegeben!");
		}
		try {

			Class.forName("com.mysql.jdbc.Driver");
			// DBS‐Verbindung herstellen
			Connection conn = DriverManager.getConnection(
					"jdbc:DATABASE",
					"USER", "PASSWORD");
			// Statement erzeugen and Query ausführen
			// SELECT Wohnung_idWohnung, SUM(Preis) from Buchung WHERE StartDate
			// > '2016-12-31' GROUP BY Buchung.Wohnung_idWohnung ASC
			// bei der Eingabe das Format YYYY-MM-DD verwenden!
			String sql = "SELECT Wohnung_idWohnung, SUM(Preis) from Buchung WHERE StartDate > ? GROUP BY Buchung.Wohnung_idWohnung ASC";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, eingabe);
			// Group By-Abfrage zu Gesamteinnahmen der Wohnungen absteigend
			// dargestellt
			ResultSet res = stmt.executeQuery();
			Integer columns = res.getMetaData().getColumnCount();
			// ResultSet verarbelten
			System.out.println("    WohnungId " + " Summe(Preis)");
			while (res.next()) {
				for (int i = 1; i <= columns; i++) {
					Object o = res.getObject(i);
					System.out.print("\t" + o);

				}
				System.out.println();
			}
			res.close();
			stmt.close();
			conn.close();
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();

		}
		in.close();
	}
}
