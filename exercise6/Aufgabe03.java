import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/*
 * Anwendungsfall 2
 */

public class Aufgabe03 {
	private static void checkInput(String[] input){
		//check number of args
		if(input.length != 6){
			System.err.println("Wrong number of arguments.");
			System.exit(-1);
		}
		//check dates
		try {
			LocalDate.parse(input[0]);
			LocalDate.parse(input[1]);
		} catch (DateTimeParseException e) {
			System.err.println("Datum nicht korrekt eingegeben!");
			System.exit(-1);
		}
		//check numbers
		try{
			Float.parseFloat(input[2]);
			Integer.parseInt(input[3]);
			Integer.parseInt(input[4]);
			Integer.parseInt(input[5]);
		} catch(Exception e){
			System.err.println("Please enter correct numbers.");
			System.exit(-1);
		}
	}
	
	//valid input: 2017-01-20, 2017-01-23, 300, 1, 1, 1
	//input, that will fail: 2017-01-20, 2017-01-23, 300, 3, 1, 1
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter the parameters for the addBuchung call."
				+ " Please use the given format. (date format: YYYY-MM-DD).");
		System.out.println("start_date, end_date, price, gast_id, wohnungs_id, transfer_id");
		String eingabe = in.nextLine();
		eingabe = eingabe.replace(" ", "");
		String[] parm = eingabe.split(",");
		//check input format
		checkInput(parm);
		try {

			Class.forName("com.mysql.jdbc.Driver");
			// DBS‐Verbindung herstellen
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://<database>",
					"<user>", "<password>");
			// call addBuchung('2017-01-20', '2017-01-23', 300, 1, 1, null, @success);
			String sql = "call addBuchung(?, ?, ?, ?, ?, ?, @success);";
			PreparedStatement stmt = conn.prepareStatement(sql);
			for(int i=0; i < parm.length; i++){
				//check if there is no transfer
				if(i == 5 && Integer.parseInt(parm[i]) == 0){
					stmt.setString(i, "null");
				} else{
					stmt.setString(i+1, parm[i]);
				}
			}
			ResultSet res = stmt.executeQuery();
			stmt = conn.prepareStatement("SELECT @success;");
			res = stmt.executeQuery();
			while (res.next()) {
				for (int i = 1; i <= res.getMetaData().getColumnCount(); i++) {
					Integer success = res.getInt(i);
					if(success == 1){
						System.out.println("Added new entry to table Buchung.");
					} else{
						System.out.println("Failed to add a new entry to table Buchung."
								+ " No payment info for the customer.");
					}

				}
			}
			//close all resources
			res.close();
			stmt.close();
			conn.close();
		} catch (SQLException | ClassNotFoundException ex) {
			ex.printStackTrace();

		}
		in.close();
	}
}