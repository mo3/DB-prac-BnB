-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Di14b_1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Di14b_1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Di14b_1` DEFAULT CHARACTER SET utf8 ;
USE `Di14b_1` ;

-- -----------------------------------------------------
-- Table `Di14b_1`.`Gast`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Di14b_1`.`Gast` (
  `idGast` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(256) NOT NULL,
  `Vorname` VARCHAR(256) NOT NULL,
  `Addresse` VARCHAR(512) NOT NULL,
  `PreisProzente` FLOAT NULL,
  PRIMARY KEY (`idGast`),
  UNIQUE INDEX `idGast_UNIQUE` (`idGast` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Di14b_1`.`Transfer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Di14b_1`.`Transfer` (
  `idTransfer` INT NOT NULL AUTO_INCREMENT,
  `StartortAddresse` VARCHAR(512) NOT NULL,
  `Datum` DATE NOT NULL,
  `ZielortAddresse` VARCHAR(512) NOT NULL,
  `Personen` INT NOT NULL,
  PRIMARY KEY (`idTransfer`),
  UNIQUE INDEX `idTransfer_UNIQUE` (`idTransfer` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Di14b_1`.`Wohnung`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Di14b_1`.`Wohnung` (
  `idWohnung` INT UNSIGNED NOT NULL COMMENT '	',
  `AnzahlZimmer` INT NOT NULL,
  `Grundpreis` FLOAT NOT NULL,
  `minPreis` FLOAT NULL,
  PRIMARY KEY (`idWohnung`),
  UNIQUE INDEX `idWohnung_UNIQUE` (`idWohnung` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Di14b_1`.`Bewertungen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Di14b_1`.`Bewertungen` (
  `idBewertungen` INT NOT NULL,
  `Wohnung_idWohnung` INT UNSIGNED NOT NULL COMMENT '	',
  `Text` VARCHAR(2048) NOT NULL,
  `StarRating` INT NOT NULL,
  `Show` TINYINT(1) NULL,
  PRIMARY KEY (`idBewertungen`),
  INDEX `fk_Bewertungen_Wohnung1_idx` (`Wohnung_idWohnung` ASC),
  UNIQUE INDEX `idBewertungen_UNIQUE` (`idBewertungen` ASC),
  CONSTRAINT `fk_Bewertungen_Wohnung1`
    FOREIGN KEY (`Wohnung_idWohnung`)
    REFERENCES `Di14b_1`.`Wohnung` (`idWohnung`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Di14b_1`.`Multiplier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Di14b_1`.`Multiplier` (
  `idMultiplier` INT NOT NULL,
  `Wohnung_idWohnung`  INT UNSIGNED NOT NULL COMMENT '	',
  `begin` DATE NOT NULL,
  `end` DATE NOT NULL,
  `precentage` FLOAT NOT NULL,
  PRIMARY KEY (`idMultiplier`),
  INDEX `fk_Multiplier_Wohnung_idx` (`Wohnung_idWohnung` ASC),
  UNIQUE INDEX `idMultiplier_UNIQUE` (`idMultiplier` ASC),
  CONSTRAINT `fk_Multiplier_Wohnung`
    FOREIGN KEY (`Wohnung_idWohnung`)
    REFERENCES `Di14b_1`.`Wohnung` (`idWohnung`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Di14b_1`.`Kontoverbindung`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Di14b_1`.`Kontoverbindung` (
  `idKundenKontoDaten` INT NOT NULL AUTO_INCREMENT,
  `Gast_idGast` INT NULL,
  `IBAN` VARCHAR(22) NOT NULL,
  `BIC` VARCHAR(14) NOT NULL,
  PRIMARY KEY (`idKundenKontoDaten`),
  INDEX `fk_KundenKontoDaten_guest1_idx` (`Gast_idGast` ASC),
  UNIQUE INDEX `idKundenKontoDaten_UNIQUE` (`idKundenKontoDaten` ASC),
  CONSTRAINT `fk_KundenKontoDaten_guest1`
    FOREIGN KEY (`Gast_idGast`)
    REFERENCES `Di14b_1`.`Gast` (`idGast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Di14b_1`.`Buchung`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Di14b_1`.`Buchung` (
  `idBuchung` INT UNSIGNED NOT NULL,
  `StartDate` DATE NOT NULL,
  `EndDate` DATE NOT NULL,
  `Preis` FLOAT NOT NULL,
  `Gast_idGast` INT NOT NULL,
  `Wohnung_idWohnung`  INT UNSIGNED NOT NULL COMMENT '	',
  `Transfer_idTransfer` INT NULL,
  PRIMARY KEY (`idBuchung`),
  INDEX `fk_Buchung_Gast1_idx` (`Gast_idGast` ASC),
  INDEX `fk_Buchung_Wohnung1_idx` (`Wohnung_idWohnung` ASC),
  INDEX `fk_Buchung_Transfer1_idx` (`Transfer_idTransfer` ASC),
  UNIQUE INDEX `idBuchung_UNIQUE` (`idBuchung` ASC),
  CONSTRAINT `fk_Buchung_Gast1`
    FOREIGN KEY (`Gast_idGast`)
    REFERENCES `Di14b_1`.`Gast` (`idGast`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Buchung_Wohnung1`
    FOREIGN KEY (`Wohnung_idWohnung`)
    REFERENCES `Di14b_1`.`Wohnung` (`idWohnung`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Buchung_Transfer1`
    FOREIGN KEY (`Transfer_idTransfer`)
    REFERENCES `Di14b_1`.`Transfer` (`idTransfer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS